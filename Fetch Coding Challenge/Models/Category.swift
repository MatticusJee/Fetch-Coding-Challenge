//
//  Category.swift
//  Fetch Coding Challenge
//
//  Created by Matt Garofola on 6/12/24.
//

import Foundation

// MARK: - CategoryResponse
struct CategoryResponse: Decodable {
    let categories: [Category]
    
    enum CodingKeys: String, CodingKey {
        case categories = "meals"
    }
}

// MARK: - Meal
struct Category: Decodable, Identifiable {
    let id = UUID()
    let name: String
    
    enum CodingKeys: String, CodingKey {
        case name = "strCategory"
    }
}
