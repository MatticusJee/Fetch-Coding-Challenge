//
//  Meal.swift
//  Fetch Coding Challenge
//
//  Created by Matt Garofola on 6/12/24.
//

import Foundation

protocol MealProtocol: Decodable, Identifiable {
    var id: String { get }
    var name: String { get }
    var thumbnailUrl: String { get }
}

// MARK: - MealListResponse
struct MealListResponse: Decodable {
    let meals: [Meal]
}

// MARK: - Meal
struct Meal: MealProtocol {
    let id: String
    let name: String
    let thumbnailUrl: String
    
    enum CodingKeys: String, CodingKey {
        case id             = "idMeal"
        case name           = "strMeal"
        case thumbnailUrl   = "strMealThumb"
    }
    
    init(from decoder: any Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.thumbnailUrl = try container.decode(String.self, forKey: .thumbnailUrl)
    }
}
