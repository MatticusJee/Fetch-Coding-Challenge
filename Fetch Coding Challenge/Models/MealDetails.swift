//
//  MealDetails.swift
//  Fetch Coding Challenge
//
//  Created by Matt Garofola on 6/12/24.
//

import Foundation

// MARK: - MealListResponse
struct MealDetailsResponse: Decodable {
    let meals: [MealDetails]
}

// MARK: - MealDetails
struct MealDetails: MealProtocol {
    let id, name: String
    let drinkAlternate: String?
    let category, area, instructions: String
    let thumbnailUrl: String
    let tags: String?
    let youtubeUrl: String
    let source: String?
    let imageSource, creativeCommonsConfirmed, dateModified: String?
    let ingredients: [Ingredient]
    
    enum CodingKeys: String, CodingKey {
        case id                         = "idMeal"
        case name                       = "strMeal"
        case drinkAlternate             = "strDrinkAlternate"
        case category                   = "strCategory"
        case area                       = "strArea"
        case instructions               = "strInstructions"
        case thumbnailUrl               = "strMealThumb"
        case tags                       = "strTags"
        case youtubeUrl                 = "strYoutube"
        case source                     = "strSource"
        case imageSource                = "strImageSource"
        case creativeCommonsConfirmed   = "strCreativeCommonsConfirmed"
        case dateModified               = "dateModified"
    }
    
    private struct DynamicCodingKeys: CodingKey {
        var stringValue: String
        init?(stringValue: String) {
            self.stringValue = stringValue
        }
        var intValue: Int?
        init?(intValue: Int) {
            return nil
        }
    }
    
    init(from decoder: any Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.drinkAlternate = try container.decodeIfPresent(String.self, forKey: .drinkAlternate)
        self.category = try container.decode(String.self, forKey: .category)
        self.area = try container.decode(String.self, forKey: .area)
        self.instructions = try container.decode(String.self, forKey: .instructions)
        self.thumbnailUrl = try container.decode(String.self, forKey: .thumbnailUrl)
        self.tags = try container.decodeIfPresent(String.self, forKey: .tags)
        self.youtubeUrl = try container.decode(String.self, forKey: .youtubeUrl)
        self.source = try container.decodeIfPresent(String.self, forKey: .source)
        self.imageSource = try container.decodeIfPresent(String.self, forKey: .imageSource)
        self.creativeCommonsConfirmed = try container.decodeIfPresent(String.self, forKey: .creativeCommonsConfirmed)
        self.dateModified = try container.decodeIfPresent(String.self, forKey: .dateModified)
        
        let dynamicIngredientContainer = try decoder.container(keyedBy: DynamicCodingKeys.self)
        var ingredientsDictionary: [Int: String] = [:]
        var measuresDictionary: [Int: String] = [:]
        for key in dynamicIngredientContainer.allKeys {
            if key.stringValue.starts(with: "strIngredient"), let index = key.stringValue.extractIndex {
                let ingredient = try dynamicIngredientContainer.decodeIfPresent(String.self, forKey: key)
                ingredientsDictionary[index] = ingredient
            } else if key.stringValue.starts(with: "strMeasure"), let index = key.stringValue.extractIndex {
                let measure = try dynamicIngredientContainer.decodeIfPresent(String.self, forKey: key)
                measuresDictionary[index] = measure
            }
        }
        
        var ingredients: [Ingredient] = []
        for (index, ingredient) in ingredientsDictionary where !ingredient.isEmpty {
            let measure = measuresDictionary[index] ?? ""
            ingredients.append(Ingredient(name: ingredient, measurement: measure))
        }

        self.ingredients = ingredients
    }
}

struct Ingredient: Identifiable {
    let id = UUID()
    let name: String
    let measurement: String
}

extension String {
    var extractIndex: Int? {
        let numberPart = self.drop(while: { !$0.isNumber })
        return Int(numberPart)
    }
}
