//
//  MealDbError.swift
//  Fetch Coding Challenge
//
//  Created by Matt Garofola on 6/12/24.
//

import Foundation

protocol MealDbError: LocalizedError {
    var title: String { get }
    var description: String { get }
    var confirmTitle: String { get }
    var dismissTitle: String { get }
}

extension MealDbError {
    var localizedDescription: String { return description }
    var errorDescription: String? { return description }
    var confirmTitle: String { return confirmTitle }
    var dismissTitle: String { return dismissTitle }
}

struct CustomMealDbError: MealDbError {
    var title: String
    var description: String
    var confirmTitle: String
    var dismissTitle: String
    
    // LocalizedError requirements
    var errorDescription: String? { return description }
}

enum ApiError: MealDbError {
    case decodeFailed(_ error: Error)
    case invalidUrl(_ url: URL?)
    
    var title: String {
        return "Api Error"
    }
    
    var description: String {
        switch self {
        case .decodeFailed(let error): return "Failed to decode response: \(error.localizedDescription)"
        case .invalidUrl(let url): return "Invalid Url: \(url?.absoluteString ?? "Empty")"
        }
    }
    
    var confirmTitle: String {
        return "OK"
    }
    
    var dismissTitle: String {
        return "Okay"
    }
}

enum HttpError: MealDbError {
    case error(HttpStatus)
    case requestFailure(Error)
    
    var title: String {
        return "Network Error"
    }
    
    var description: String {
        switch self {
        case .error(let status):
            return getMessage(for: status)
        case .requestFailure(let error):
            return error.localizedDescription
        }
    }
    
    var confirmTitle: String {
        return "OK"
    }
    
    var dismissTitle: String {
        return "Okay"
    }
    
    var isRestricted: Bool {
        if case let HttpError.error(status) = self {
            return status.isRestricted
        }
        return false
    }
    
    private func getMessage(for httpStatus: HttpStatus) -> String {
        switch httpStatus {
        case .noContent: return "Response has no Content"
        case .multipleChoices: return "Response has multiple choices"
        case .restricted: return "User Restricted"
        case .serviceRestricted: return "Service Restricted"
        case .badRequest: return "Bad request"
        case .unauthorized: return "User is unauthorized to complete this request"
        case .paymentRequired: return "Payment is required in order to complete this request"
        case .forbidden: return "User is forbidden to complete this request"
        case .notFound: return "Endpoint not found"
        case .methodNotAllowed: return "This method is not allowed"
        case .nonAcceptable: return "Request is not acceptable"
        case .requestTimeout: return "Request has timed out"
        case .internalServerError: return "Internal Server Error"
        default: return "No Data"
        }
    }
}
