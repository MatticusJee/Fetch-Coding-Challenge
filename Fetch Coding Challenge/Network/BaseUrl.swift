//
//  BaseUrl.swift
//  Fetch Coding Challenge
//
//  Created by Matt Garofola on 6/12/24.
//

import Foundation

enum BaseUrl {
    case mealDb
}

extension BaseUrl {
    var host: String {
        switch self {
        case .mealDb:
            return "themealdb.com"
        }
    }
    
    var components: URLComponents {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = self.host
        return urlComponents
    }
}
