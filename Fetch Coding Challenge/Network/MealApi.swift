//
//  MealApi.swift
//  Fetch Coding Challenge
//
//  Created by Matt Garofola on 6/12/24.
//

import Foundation

enum MealApi: Api {
    case categories
    case mealList(category: String)
    case mealDetails(id: String)
    
    var baseUrl: BaseUrl {
        return .mealDb
    }
    
    var method: HttpMethod { return .GET }
    
    var path: String {
        switch self {
        case .categories:
            return "/api/json/v1/1/list.php"
        case .mealList:
            return "/api/json/v1/1/filter.php"
        case .mealDetails:
            return "/api/json/v1/1/lookup.php"
        }
    }
    
    var queryItems: [URLQueryItem]? {
        switch self {
        case .categories:
            return [URLQueryItem(name: "c", value: "list")]
        case let .mealList(category):
            return [URLQueryItem(name: "c", value: category)]
        case let .mealDetails(id):
            return [URLQueryItem(name: "i", value: id)]
        }
    }
    
    var body: Data? { return nil }
}
