//
//  HttpStatus.swift
//  Fetch Coding Challenge
//
//  Created by Matt Garofola on 6/12/24.
//

import Foundation

enum HttpStatus: Int {
    case ok                     = 200
    case created                = 201
    case accepted               = 202
    case noContent              = 204
    case multipleChoices        = 300
    case restricted             = 301
    case serviceRestricted      = 307
    case badRequest             = 400
    case unauthorized           = 401
    case paymentRequired        = 402
    case forbidden              = 403
    case notFound               = 404
    case methodNotAllowed       = 405
    case nonAcceptable          = 406
    case requestTimeout         = 408
    case internalServerError    = 500
    case noData
    
    var isSuccessful: Bool {
        switch self {
        case .ok, .created, .accepted:
            return true
        default:
            return false
        }
    }
    
    var successMsg: String? {
        switch self {
        case .ok: return "Request successful"
        case .accepted: return "Request accepted"
        case .created: return "Request created"
        default: return nil
        }
    }
    
    var isRestricted: Bool {
        self == .restricted || self == .serviceRestricted
    }
    
    var error: HttpError {
        HttpError.error(self)
    }
}

enum HttpMethod: String {
    case DELETE
    case GET
    case PATCH
    case POST
    case PUT
}
