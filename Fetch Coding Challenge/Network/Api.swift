//
//  Api.swift
//  Fetch Coding Challenge
//
//  Created by Matt Garofola on 6/12/24.
//

import Foundation

protocol Api {
    var method: HttpMethod { get }
    var baseUrl: BaseUrl { get }
    var path: String { get }
    var queryItems: [URLQueryItem]? { get }
    var components: URLComponents { get }
    var fullUrl: URL { get }
    var urlRequest: URLRequest { get }
}

extension Api {
    typealias StatusCode = Int
    
    var components: URLComponents {
        var urlComponents = self.baseUrl.components
        urlComponents.path = self.path
        urlComponents.queryItems = self.queryItems
        return urlComponents
    }
    
    var fullUrl: URL {
        guard let url = self.components.url else {
            fatalError(ApiError.invalidUrl(nil).localizedDescription)
        }
        return url
    }
    
    var urlRequest: URLRequest {
        var request = URLRequest(url: self.fullUrl)
        request.httpMethod = self.method.rawValue
        return request
    }
    
    func request() async throws -> (Data, StatusCode) {
        do {
            let (data, urlResponse) = try await URLSession.shared.data(for: self.urlRequest)
            let statusCode = (urlResponse as? HTTPURLResponse)?.statusCode ?? 0
            let httpStatus = HttpStatus(rawValue: statusCode) ?? .noData
            
            guard httpStatus.isSuccessful else {
                throw getError(data: data, httpStatus: httpStatus)
            }
            return (data, statusCode)
        } catch let error {
            throw error is MealDbError ? error : HttpError.requestFailure(error)
        }
    }
    
    func requestData<T: Decodable>(_ responseType: T.Type) async throws -> T {
        let (data, statusCode) = try await request()
        return try getDecodedResponse(responseType, data: data, statusCode: statusCode)
    }
    
    private func getDecodedResponse<T: Decodable>(_ responseType: T.Type, data: Data, statusCode: StatusCode) throws -> T {
        do {
            return try JSONDecoder().decode(responseType, from: data)
        } catch let error {
            throw ApiError.decodeFailed(error)
        }
    }
    
    private func getError(data: Data, httpStatus: HttpStatus) -> MealDbError {
        let error: MealDbError = httpStatus.error
        // Log Error with analytics tool like Sentry
        return error
    }
}
