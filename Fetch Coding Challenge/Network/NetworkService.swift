//
//  NetworkService.swift
//  Fetch Coding Challenge
//
//  Created by Matt Garofola on 6/12/24.
//

import Foundation

struct NetworkService {
    static func getListOfCategories() async throws -> [Category] {
        return try await MealApi.categories.requestData(CategoryResponse.self).categories
    }
    
    static func getListOfMeals(by category: Category) async throws -> [Meal] {
        return try await MealApi.mealList(category: category.name).requestData(MealListResponse.self).meals
    }
    
    static func getMealDetails(for mealId: String) async throws -> MealDetails {
        guard let details = try await MealApi.mealDetails(id: mealId).requestData(MealDetailsResponse.self).meals.first else {
            throw CustomMealDbError(title: "Oh no...", description: "Could not get details, try again", confirmTitle: "OK", dismissTitle: "Understood")
        }
        return details
    }
}
