//
//  Helpers.swift
//  Fetch Coding Challenge
//
//  Created by Matt Garofola on 6/22/24.
//

import Foundation
import SwiftUI
import Combine

struct LoadingView: View {
    var body: some View {
        VStack {
            ProgressView()
                .progressViewStyle(CircularProgressViewStyle())
            Text("Loading...")
        }
    }
}

struct LazyView<Content: View>: View {
    let build: () -> Content
    
    init(_ build: @autoclosure @escaping () -> Content) {
        self.build = build
    }

    var body: Content {
        build()
    }
}

#Preview {
    ContentView()
}

// Extension for adding delay to retries
extension Publisher {
    func retry(withDelay delay: TimeInterval, retries: Int? = nil) -> AnyPublisher<Output, Failure> {
        self.catch { _ in
            if let retries {
                return Just(()).delay(for: .seconds(delay), scheduler: RunLoop.main)
                    .flatMap { _ in self }
                    .retry(retries - 1)
                    .eraseToAnyPublisher()
            } else {
                return Just(()).delay(for: .seconds(delay), scheduler: RunLoop.main)
                    .flatMap { _ in self.retry(withDelay: delay) }
                    .eraseToAnyPublisher()
            }
        }
        .eraseToAnyPublisher()
    }
}
