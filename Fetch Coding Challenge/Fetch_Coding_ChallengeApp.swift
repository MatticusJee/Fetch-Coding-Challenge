//
//  Fetch_Coding_ChallengeApp.swift
//  Fetch Coding Challenge
//
//  Created by Matt Garofola on 6/12/24.
//

import SwiftUI

@main
struct Fetch_Coding_ChallengeApp: App {
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
