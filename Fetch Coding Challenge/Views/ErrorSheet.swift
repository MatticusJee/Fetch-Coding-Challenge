//
//  ErrorSheet.swift
//  Fetch Coding Challenge
//
//  Created by Matt Garofola on 6/12/24.
//

import SwiftUI

struct ErrorSheet: View {
    @Environment(\.dismiss) var dismissAction
    @Binding var bottomSheetHeight: CGFloat // Binding to receive height updates
    @Binding var alertDisplay: AlertDisplay?
    @State private var animationCount = 1
    @State private var animate = true
    @State private var timer: Timer? = nil

    var body: some View {
        VStack {
            Text(alertDisplay?.title ?? "")
                .font(.title)
                .fontWeight(.bold)
                .multilineTextAlignment(.center)
                .frame(maxWidth: .infinity, alignment: .center)
                .padding(.vertical)
                .padding(.horizontal, 16)
            
            if #available(iOS 17.0, *) {
                Image(systemName: "exclamationmark.brakesignal")
                    .renderingMode(.original)
                    .resizable(resizingMode: .stretch)
                    .aspectRatio(contentMode: .fit)
                    .padding(.vertical)
                    .padding(.horizontal, 48)
                    .symbolEffect(.bounce, options: .repeating, value: animationCount)
                    .onAppear {
                        startAnimation()
                    }
                    .onDisappear {
                        stopAnimation()
                    }
            } else {
                Image(systemName: "exclamationmark.brakesignal")
                    .renderingMode(.original)
                    .resizable(resizingMode: .stretch)
                    .aspectRatio(contentMode: .fit)
                    .padding(.vertical)
                    .padding(.horizontal, 48)
                    .onAppear {
                        startAnimation()
                    }
                    .onDisappear {
                        stopAnimation()
                    }
            }
            
            Text(alertDisplay?.message ?? "")
                .font(.title2)
                .fontWeight(.bold)
                .multilineTextAlignment(.center)
                .frame(maxWidth: .infinity, alignment: .center)
                .padding(.vertical)
                .padding(.horizontal, 16)
            
            Button {
                print("OK")
                dismissAction()
            } label: {
                Text(alertDisplay?.dismissTitle ?? "")
                    .font(.title3)
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                    .multilineTextAlignment(.center)
                    .lineLimit(1)
                    .minimumScaleFactor(0.5)
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .padding(.vertical)
                    .padding(.horizontal, 16)
            }
            .background(.red)
            .clipShape(Capsule())
        }
        .fixedSize(horizontal: false, vertical: true)
        .padding(.all)
        .background(GeometryReader { geometry in
            Color.white
                .onAppear {
                    DispatchQueue.main.async {
                        print(geometry.size.height)
                        bottomSheetHeight = geometry.size.height
                    }
                }
        })
    }
    
    private func startAnimation() {
        timer = Timer.scheduledTimer(withTimeInterval: 0, repeats: true) { _ in
            animationCount += 1
        }
    }
    
    private func stopAnimation() {
        timer?.invalidate()
        timer = nil
        animationCount = 0
    }
}

struct AlertDisplay: Identifiable {
    let id = UUID()
    let title: String
    let message: String
    let dismissTitle: String
}

struct ErrorSheet_Previews: PreviewProvider {
    @State static var bottomSheetHeight: CGFloat = 0
    @State static var errorAlertDisplay: AlertDisplay? = AlertDisplay(title: "Uh oh...",
                                                                      message: "Network Error, please try again",
                                                                      dismissTitle: "OK")
    
    static var previews: some View {
        let errorSheet = ErrorSheet(bottomSheetHeight: $bottomSheetHeight, alertDisplay: $errorAlertDisplay)
        return errorSheet
    }
}
