//
//  ContentView.swift
//  Fetch Coding Challenge
//
//  Created by Matt Garofola on 6/12/24.
//

import SwiftUI

struct ContentView: View {
    @State private var searchIsPresented = false
    @State private var bottomSheetHeight: CGFloat = 0
    
    @Bindable private var viewModel = MealListViewModel()
    
    var body: some View {
        Group {
            NavigationStack {
                if !viewModel.mealResults.isEmpty {
                    generateList(meals: viewModel.mealResults)
                } else {
                    LoadingView()
                }
            }
        }
        .sheet(item: $viewModel.errorSheet, onDismiss: {
            print()
        }, content: { _ in
            ErrorSheet(bottomSheetHeight: $bottomSheetHeight, alertDisplay: $viewModel.errorSheet)
                .presentationDetents([.height(bottomSheetHeight)])
                .presentationDragIndicator(.visible)
        })
    }
    
    /// When the API call is successful, this function is called to generate the List View
    /// - Parameter meals: The sorted API response of meals based on the category selected
    /// - Returns: The list view structure
    func generateList(meals: [Meal]) -> some View {
        List(viewModel.mealResults) { meal in
            NavigationLink {
                LazyView(MealDetailsView(selectedMeal: meal))
                    .navigationBarTitleDisplayMode(.inline)
            } label: {
                // TODO: 'Peak' only shows a Loading window
                Text(meal.name)
                    .contextMenu {
                       NavigationLink(destination: LazyView(MealDetailsView(selectedMeal: meal))) {
                           LazyView(MealDetailsView(selectedMeal: meal))
                       }
                    }
            }
        }
        .listStyle(.automatic)
        .navigationBarTitleDisplayMode(.automatic)
        .navigationTitle("^[\(meals.count) \(viewModel.selectedCategory?.name ?? "") Meal](inflect: true)")
        .toolbarBackground(.background, for: .navigationBar)
        .refreshable {
            viewModel.fetchMeals()
        }
        .searchable(text: $viewModel.searchText, placement: .navigationBarDrawer)
        .toolbar {
            ToolbarItem(placement: .topBarTrailing) {
                Menu {
                    Menu("Change Category") {
                        ForEach(viewModel.categories) { category in
                            Button(category.name) {
                                selected(new: category)
                            }
                        }
                    }
//                    Button("Switch Layouts") {
//                        // TODO: Show a different kind of list
//                    }
                } label: {
                    Image(systemName: "slider.horizontal.3")
                }
            }
        }
    }
    
    /// Reassigns the selected category after the user picks one from the Menu
    /// - Parameter category: The struct that contains the category name
    func selected(new category: Category) {
        viewModel.selectedCategory = category
        viewModel.fetchMeals()
    }
}
