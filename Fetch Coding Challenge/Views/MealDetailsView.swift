//
//  MealDetailsView.swift
//  Fetch Coding Challenge
//
//  Created by Matt Garofola on 6/12/24.
//

import SwiftUI
import Kingfisher

struct MealDetailsView<MealType: MealProtocol>: View {
    @State private var bottomSheetHeight: CGFloat = 0
    
    @StateObject private var viewModel: MealDetailsViewModel<MealType>
    var selectedMeal: MealType
    
    init(selectedMeal: MealType) {
        self.selectedMeal = selectedMeal
        self._viewModel = StateObject(wrappedValue: MealDetailsViewModel(selectedMeal: selectedMeal))
    }
    
    var body: some View {
        Group {
            if let details = viewModel.mealDetails {
                generateDetailsView(details: details)
            } else {
                LoadingView()
            }
        }
        .sheet(item: $viewModel.errorSheet, onDismiss: {
            print()
        }, content: { _ in
            ErrorSheet(bottomSheetHeight: $bottomSheetHeight, alertDisplay: $viewModel.errorSheet)
                .presentationDetents([.height(bottomSheetHeight)])
                .presentationDragIndicator(.visible)
        })
        .onDisappear {
            viewModel.mealDetails = nil
        }
        .padding()
    }
    
    private func generateDetailsView(details: MealDetails) -> some View {
        ScrollView {
            Text(details.name)
                .font(.title)
                .fontWeight(.bold)
                .multilineTextAlignment(.center)
                .lineLimit(2)
                .minimumScaleFactor(0.5)
            KFImage.url(URL(string: details.thumbnailUrl))
                .resizable(resizingMode: .stretch)
                .aspectRatio(1.0, contentMode: .fit)
            Divider()
            Text("Ingredients")
                .font(.title)
                .fontWeight(.bold)
                .multilineTextAlignment(.leading)
                .minimumScaleFactor(0.5)
                .lineLimit(1)
                .frame(maxWidth: .infinity, alignment: .leading)
            ForEach(details.ingredients) { ingredient in
                Text("\(ingredient.measurement) - \(ingredient.name)")
                    .font(.title3)
                    .fontWeight(.regular)
                    .multilineTextAlignment(.leading)
                    .lineLimit(1)
                    .minimumScaleFactor(0.5)
                    .frame(maxWidth: .infinity, alignment: .leading)
            }
            Divider()
            Text("Directions")
                .font(.title)
                .fontWeight(.bold)
                .multilineTextAlignment(.leading)
                .minimumScaleFactor(0.5)
                .lineLimit(1)
                .frame(maxWidth: .infinity, alignment: .leading)
            Text(details.instructions)
                .font(.title3)
                .fontWeight(.regular)
                .multilineTextAlignment(.leading)
                .frame(maxWidth: .infinity, alignment: .leading)
        }
        .scrollIndicators(.hidden)
    }
}

#Preview {
    MealDetailsView(selectedMeal: MockMeal())
}

struct MockMeal: MealProtocol {
    var id: String = "53049"
    var name: String = "Apam balik"
    var thumbnailUrl: String = "https://www.themealdb.com/images/media/meals/adxcbq1619787919.jpg"
}
