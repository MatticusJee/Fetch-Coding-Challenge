//
//  MealListViewModel.swift
//  Fetch Coding Challenge
//
//  Created by Matt Garofola on 6/12/24.
//

import Foundation
import Observation

@Observable final class MealListViewModel {
    var categories: [Category] = []
    var meals: [Meal] = []
    var selectedCategory: Category?
    var errorSheet: AlertDisplay?
    var searchText: String = ""
    
    var mealResults: [Meal] {
        if searchText.isEmpty {
            return meals
        } else {
            return meals.filter { $0.name.contains(searchText) }
        }
    }
    
    init() {
        startUp()
    }

    func startUp() {
        print("Fetching Categories and Meals...")
        fetchCategories { result in
            switch result {
            case .success:
                self.fetchMeals()
            case .failure(let failure):
                print(failure)
            }
        }
    }
    
    private func fetchCategories(handler: @escaping (Result<[Category], CustomMealDbError>) -> Void) {
        print("Fetching Categories...")
        
        Task {
            do {
                let categories = try await NetworkService.getListOfCategories()
                let sortedCategories = categories.sorted(by: { $0.name < $1.name })
                DispatchQueue.main.async {
                    self.categories = sortedCategories
                    self.selectedCategory = sortedCategories.first(where: { $0.name == "Dessert" })
                    handler(.success(sortedCategories))
                    print("Found Categories!")
                }
            } catch {
                let customError: CustomMealDbError
                if let error = error as? CustomMealDbError {
                    customError = error
                    print("Error Categories 1!")
                } else {
                    customError = CustomMealDbError(
                        title: "Error",
                        description: error.localizedDescription,
                        confirmTitle: "OK",
                        dismissTitle: "Understood"
                    )
                    print("Error Categories 2!")
                }
                DispatchQueue.main.async {
                    self.errorSheet = AlertDisplay(title: customError.title, message: customError.localizedDescription, dismissTitle: "Understood")
                    handler(.failure(customError))
                }
            }
        }
    }
    
    func fetchMeals() {
        print("Fetching Meal List...")
        
        Task {
            do {
                guard let selectedCategory = selectedCategory else {
                    throw CustomMealDbError(
                        title: "Uh Oh...",
                        description: "Category was not found, please try again.",
                        confirmTitle: "OK",
                        dismissTitle: "Understood"
                    )
                }
                let meals = try await NetworkService.getListOfMeals(by: selectedCategory)
                let sortedMeals = meals.sorted(by: { $0.name < $1.name })
                DispatchQueue.main.async {
                    self.meals = sortedMeals
                    print("Found Meals!")
                    print("New Meal List...")
                }
            } catch {
                let customError: CustomMealDbError
                if let error = error as? CustomMealDbError {
                    customError = error
                    print("Error Meals 1!")
                } else {
                    customError = CustomMealDbError(
                        title: "Error",
                        description: error.localizedDescription,
                        confirmTitle: "OK",
                        dismissTitle: "Understood"
                    )
                    print("Error Meals 2!")
                }
                DispatchQueue.main.async {
                    self.errorSheet = AlertDisplay(title: customError.title, message: customError.localizedDescription, dismissTitle: "Understood")
                }
            }
        }
    }
}
