//
//  MealDetailsViewModel.swift
//  Fetch Coding Challenge
//
//  Created by Matt Garofola on 6/12/24.
//

import Foundation
import Combine

final class MealDetailsViewModel<MealType: MealProtocol>: ObservableObject {
    @Published var mealDetails: MealDetails?
    @Published var errorSheet: AlertDisplay?
    
    private var cancellables = Set<AnyCancellable>()
    
    init(selectedMeal: any MealProtocol) {
        fetchMealDetails(by: selectedMeal.id)
    }
    
    private func fetchMealDetails(by id: String) {
        print("Fetching Meal Details for Meal ID: \(id)")
        
        return Future<MealDetails, CustomMealDbError> { promise in
            Task {
                do {
                    let mealDetails = try await NetworkService.getMealDetails(for: id)
                    promise(.success(mealDetails))
                } catch {
                    if let error = error as? CustomMealDbError {
                        promise(.failure(error))
                    } else {
                        promise(.failure(CustomMealDbError(
                            title: "Error",
                            description: error.localizedDescription,
                            confirmTitle: "OK",
                            dismissTitle: "Understood"
                        )))
                    }
                }
            }
        }
        .eraseToAnyPublisher()
        .retry(withDelay: 10, retries: 0)
        .receive(on: DispatchQueue.main) // Using this as opposed to @MainActor for no valid alternative reason than to know this is an alternative option
        .sink { completion in
            if case let .failure(error) = completion {
                self.errorSheet = AlertDisplay(title: error.title, message: error.localizedDescription, dismissTitle: "Understood")
            }
        } receiveValue: { mealDetails in
            self.mealDetails = mealDetails
        }
        .store(in: &cancellables)
    }
}
