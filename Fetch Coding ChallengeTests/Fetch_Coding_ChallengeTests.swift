//
//  Fetch_Coding_ChallengeTests.swift
//  Fetch Coding ChallengeTests
//
//  Created by Matt Garofola on 6/12/24.
//

import XCTest
@testable import Fetch_Coding_Challenge

final class Fetch_Coding_ChallengeTests: XCTestCase {
    
    // TODO: Getting "'Category' is ambiguous for type lookup in this context" error
//    var category: Category!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testFetchingCategoriesApiOnSuccess() async throws {
        let response = try await NetworkService.getListOfCategories()
        let selectedCategory = response.first(where: { $0.name == "Dessert" })
        XCTAssertNotNil(selectedCategory)
    }

    func testFetchingListOfMealsApiOnSuccess() async throws {
        let categoryResponse = try await NetworkService.getListOfCategories()
        let selectedCategory = categoryResponse.first(where: { $0.name == "Dessert" })
        let listOfMealsResponse = try await NetworkService.getListOfMeals(by: selectedCategory!)
        XCTAssertEqual(listOfMealsResponse.count, 65)
    }
    
    func testFetchingMealDetailsApiOnSuccess() async throws {
        let categoryResponse = try await NetworkService.getListOfCategories()
        let selectedCategory = categoryResponse.first(where: { $0.name == "Dessert" })
        let listOfMealsResponse = try await NetworkService.getListOfMeals(by: selectedCategory!)
        let mealDetailsResponse = try await NetworkService.getMealDetails(for: listOfMealsResponse.first!.id)
        XCTAssertEqual(mealDetailsResponse.id, listOfMealsResponse.first!.id)
    }
    
    // TODO: Failure cases

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
